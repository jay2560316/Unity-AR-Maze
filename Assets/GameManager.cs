using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject FinishUI;
    private GameObject Ball;
    
    private float endTime;
    private float timer;
    bool isFinished = false;
    void Start()
    {
        Ball = GameObject.Find("Ball");
        FinishUI = GameObject.Find("FinishUI");
        FinishUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFinished)
        {
            timer = Time.time - endTime;
            if (timer > 5)
            {
                reset();
                isFinished = false;
            }
        }
        

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            FinishUI.SetActive(true);
            Debug.Log("Player has entered Finish Line!");
            endTime = Time.time;
            isFinished = true;
        }
    }

    void reset()
    {
        FinishUI.SetActive(false);
        Ball.transform.position = new Vector3(-0.415f, 0.06f, 0.415f);
    }
}
